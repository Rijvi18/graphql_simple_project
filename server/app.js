const express = require('express');
const dotenv = require('dotenv');
const {graphqlHTTP} = require('express-graphql');
const schema = require('./schema/schema')
const mongoose= require('mongoose');
const cors = require('cors');
const path = require('path');




const app= express();

//allow cors origin request
app.use(cors());

mongoose.connect('mongodb+srv://admin:admin@cluster0.yyzmw.mongodb.net/users?retryWrites=true&w=majority',{useNewUrlParser: true, useUnifiedTopology: true});
mongoose.connection.once('open',()=>{
    console.log('Connected to the database');
});

dotenv.config( { path : 'config.env'} )
const PORT = process.env.PORT || 8080

app.use('/graphql',graphqlHTTP({
    schema,
    graphiql:true
}));

app.listen(PORT, ()=> { console.log(`Server is running on http://localhost:${PORT}`)});
